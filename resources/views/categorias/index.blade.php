@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Categoria Productos</span>
                        @can('categorias.create')
                        <a href="{{url('categorias/create')}}" class="btn btn-sm  pull-right">
                               <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nueva Categoria
                              </span>
                         </a>
                         @endcan
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('categorias.search')
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Desc_categoria</th>  
                                    <th >Estado</th> 
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($categorias as $categoria)
                   		<tr>
                   			<td>{{$categoria->cod_categoria}}</td>
                   			<td>{{$categoria->desc_categoria}}</td>
                                    <td>{{$categoria->estado}}</td>
                   			
                   			<td>
                   				@can('categorias.edit')
                   				<a href="{{action('CategoriaController@edit',$categoria->cod_categoria)}}"><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('categorias.destroy')
                   				<a href="" data-target="#modal-delete-{{$categoria->cod_categoria}}" data-toggle="modal"><span style="font-size: 20px; "><i class="fa fa-trash"></i></span></a>
                   				@endcan
                   			</td>
                   		</tr>
                              @include('categorias.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$categorias->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection