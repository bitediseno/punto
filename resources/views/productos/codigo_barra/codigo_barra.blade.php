@extends('layouts.principal')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">BarCodes
                	</div>
                <div class="panel-body">
                   @foreach ($producto as $product)
					<div>
					{!!DNS1D::getBarcodeHTML($product->codigo,"C128")!!}
	
						</div>
					<div style="padding-bottom: 5px;width: 100px;">
						{{$product->nombre_producto}}
		
					</div>
		
		
					@endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<style>
	
	.code{

		height: 80px;
	}
</style>
@endsection