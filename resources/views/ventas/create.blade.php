@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
        <a href="{{url('ventas')}}">Ventas</a>
        <i class="fa fa-angle-right"></i>
				<span>Nueva Venta</span>
                        
			</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
      
 	<div class="banner">
	
           @if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                              @endforeach

                        </ul>

                  </div>
                  @endif
            <form action="{{url('ventas')}}" method="POST" enctype="multipart/form-data">
        
              {{csrf_field()}}
                  <div class="col-sm-6">
                    <div class="form-group">
                      
                    <label for="cliente">Clientes</label>
               
                    <select name="cod_cliente" id="cod_cliente" class="form-control selectpicker" data-live-search="true">
                      @foreach($clientes as $cliente)
                      <option value="{{$cliente->cod_cliente}}">{{$cliente->nombres}} - {{$cliente->ruc}} </option>
                      @endforeach
                    </select>
                  </div>
                    </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                    <label for="tipo_venta">Tipo Venta</label>
                    <select name="cod_tipo_venta" id="cod_tipo_venta" class="form-control selectpicker" data-live-search="true">
                      @foreach($tipo_ventas as $tipo_venta)
                      <option value="{{$tipo_venta->cod_tipo_venta}}">{{$tipo_venta->desc_tipo_venta}} </option>
                      @endforeach
                    </select>
                    </div>    
                  </div>
                  
      <div class="col-sm-4">
               <div class="form-group">
                          <label for="numero_factura">Factura Nro</label>
                          <input type="text" name="numero_factura" required  class="form-control" placeholder="Numero de Factura..">
              </div>
      </div>    
      <div class="clearfix"> </div>
    <div class="row">
               <div class="col-sm-4">
              <div class="form-group">
                <label>Articulo</label>
                <select name="pidarticulo" class="form-control selectpicker" id="pidarticulo" data-live-search="true">
                  <option value="">Seleccion articulo</option>
                @foreach ($productos as $producto)
                  <option value="{{$producto->cod_producto}}_{{$producto->precio_venta}}_{{$producto->precio_compra}}">{{$producto->producto}}</option>
                @endforeach   

                </select>


              </div>

            </div>
            <div class="col-sm-1">
              <div class="form-group">
                <label for="cantidad">Cantidad</label>
                <input type="number"  name="pcantidad" id="pcantidad" class="form-control" value="1" min="1" step="1">
              </div>
            </div>
           
            <div class="col-sm-2">
              <div class="form-group">
                <label for="precio_venta">Precio venta</label>
                <input type="number"  name="pprecio_venta" id="pprecio_venta" class="form-control" placeholder="Precio Venta" readonly>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="form-group">
                <label for="precio_venta">Descuento</label>
                <input type="number"  name="pdescuento" id="pdescuento" class="form-control" value="0">

              </div>


            </div>
            <div class="col-lg-2" >
              <br>
            <button type="button" id="bt_add" class="btn btn-sm btn-primary">Agregar</button>
           </div>

            
              <div class="form-group">
                
              </div>
           
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
              
              <table id="detalles" class="table table-striped table-bordered table-condensed">
                <thead>
                  <th>Opciones</th>
                  <th>Articulos</th>
                  <th>Cantidad</th>
                  <th>Precio venta</th>
                  <th>Descuento</th>
                  <th>Sub total</th>
                </thead>
                <tfoot>
                  <th>Total</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th><h4 id="total">Gs 0</h4><input type="hidden" name="total_venta" id="total_venta">
                   </th>
                </tfoot>
              </table>

            </div>
        </div><br>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
          <div class="form-group">
          <button class="btn btn-primary" type="submit">Guardar</button>
          <button class="btn btn-danger" type="reset">Cancelar</button>
        </div>
        </div> 
              </form>
  




  <div class="clearfix"> </div>


	@push('scripts')
  <script> 
          $(document).ready(function(){
     
                //$('#datetimepicker1').datetimepicker();
           // alert("ok");

            })
          var cont=0;
          total=0;
          subtotal=[];
          $("#guardar").hide();
          $("#pidarticulo").change(function(){

            mostrarValores();

          });

          


          function mostrarValores(){
            
            datosArticulo=document.getElementById('pidarticulo').value.split('_');
            $("#pprecio_venta").val(datosArticulo[1]);
         
            
          }

          function agregar(){
            datosArticulos=document.getElementById('pidarticulo').value.split('_');
            idarticulo=datosArticulos[0];
            //idarticulo=$("#pidarticulo").val();
            articulo=$("#pidarticulo option:selected").text();
            cantidad=$("#pcantidad").val();
            precio_venta=$("#pprecio_venta").val();
            descuento=$("#pdescuento").val();
            

            if(idarticulo!="" && cantidad!="" && cantidad>0 &&  precio_venta!="")
            {

              subtotal[cont]=(cantidad* precio_venta)-descuento;
              //total=total+subtotal[cont];
              var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-danger" onClick="eliminar('+cont+');"><i class="fa fa-trash"></i></button></td><td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'" style="width:60px;">'+articulo+'</td><td><input type="number" name="cantidad[]" id="cantidad'+cont+'" onChange="actualizar_cantidad('+cont+');" value="'+cantidad+'" style="width:60px;" min="1" step="1"></td><td><input type="number" name="precio_venta[]" id="precio_venta'+cont+'" value="'+precio_venta+'" readonly></td><td><input type="number" name="descuento[]" value="'+descuento+'"></td><td  class="subtotal" name="subtotal[]" id="subtotal'+cont+'" >'+subtotal[cont]+'</td></tr>';
              
              cont++;
              limpiar();

              //$("#total").html("Gs"+total);
              //$("#total_compra").val(total);
             
             
              $("#detalles").append(fila);
               totales();
                evaluar();
            }
            else
            {
              alert("Error al ingresar el articulo");
            }
          }
          function limpiar(){
            $("#pcantidad").val("");
            $("#pprecio_venta").val("");
          }
          function evaluar() {
            if (total>0)
            {
              $("#guardar").show();
            }
            else
            {
              $("#guardar").hide();
            } 

          }
          function eliminar(index){
            total=total-subtotal[index];
            $("#total").html("Gs"+ total);
            $("#fila"+ index).remove();
            evaluar();
          }
          $("#bt_add").click(function(){
              
            
              agregar();
            })
          function totales(){
            var sum=0;
            $('.subtotal').each(function() {  
              sum += parseFloat($(this).text().replace(/,/g, ''), 10);  
              }); 
           $('#total').html("Gs"+sum.toFixed(0));
           $("#total_venta").val(sum.toFixed(0));
           total=sum.toFixed(0);
          }

          function actualizar_cantidad(index){

            var cant = $("#cantidad" + index).val();
            precio=$("#precio_venta" + index).val();
            $("#subtotal"+ index).html( cant*precio);

             // alert(cant*precio);
             totales();
            
         
          }
        </script>
  @endpush

@endsection
