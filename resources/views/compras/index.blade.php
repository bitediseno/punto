@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Compras</span>
                        @can('compras.create')
                        <a href="{{url('compras/create')}}" Class="pull-right">
                              
                              <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nueva Compra
                              </span>
                         </a>
                         @endcan
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('compras.search')
			<div class="col-md-12 ">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      
                                    <th >Fecha</th>  
                                    <th >Cantidad</th>  
                                    <th >Tipo Comprobante</th> 
                                    <th >Numero</th> 
                                    <th >Precio Compra</th>
                                    <th >Total</th>
                                    <th >Estado</th>
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($compras as $compra)
                   		<tr>
                   			<td>{{$compra->cod_compra}}</td>

                   			<td>{{ \Carbon\Carbon::parse($compra->fecha_compra)->format('d/m/Y')}}</td>
                                    <td>{{$compra->cantidad}}</td>
                                    <td>{{$compra->desc_tipo_comprobante}}</td>
                                    <td>{{$compra->numero_comprobante}}</td>
                                    <td>{{$compra->precio_compra}}</td>
                                    <td>{{$compra->total_compra}}</td>
                                    <td>{{$compra->estado}}</td>
                   			<td>
                   				@can('compras.show')
                   				<a href="{{url('compras',$compra->cod_compra)}}"  name="compra"><span style="font-size: 20px;"><i class="fa fa-info"></i></span></a>
                   				@endcan
                   			</td>
                                    
                                    <td>
                   			       @can('compras.destroy')         				
                   				<a href="" data-target="#modal-delete-{{$compra->cod_compra}}" data-toggle="modal" >
                                                <span style="font-size: 20px; "><i class="fa fa-trash"></i></span></a>
                                          @endcan
                   				
                                    </td>
                   			
                   		</tr>
                              @include('compras.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$compras->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection