@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Modificar Clientes</span>
                        
			</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
			<div class="col-md-12 ">
			<div class="content-top-1">
				@if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                              @endforeach

                        </ul>

                  </div>
                  @endif
				 <form action="{{url('clientes',[$cliente->cod_cliente])}}" method="POST" enctype="multipart/form-data">
				{{method_field('PUT')}}
				{{csrf_field()}}
                		
                		<div class="form-group">
                    <label for="name">Razon Social</label>
                    <input type="text" name="razon_social" required  class="form-control" value="{{$cliente->nombres}}">
        </div>
        <div class="form-group">
                    <label for="nombre">Ruc</label>
                    <input type="text" name="ruc" required  class="form-control" value="{{$cliente->ruc}}">
        </div>
         <div class="form-group">
                    <label for="nombre">Email</label>
                    <input type="text" name="email" required  class="form-control" value="{{$cliente->correo}}">
        </div>
         <div class="form-group">
                    <label for="nombre">Telefono</label>
                    <input type="text" name="telefono" required  class="form-control" value="{{$cliente->telefono}}">
        </div>
        <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                	</form>
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection
