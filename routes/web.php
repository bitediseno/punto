<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    $user=Auth::user();
    $roles = auth()->user()->roles;
    echo $roles;
});*/


//codigo de barras
Route::get('productos/codigo_barra','BarcodeController@index')->name('codigo_barra');

Route::resource('proveedores','ProveedorController');
Route::resource('marcas','MarcaController');
Route::resource('categorias','CategoriaController');
Route::resource('clientes','ClienteController');
Route::resource('productos','ProductoController');
Route::resource('compras','CompraController');
Route::resource('ventas','VentaController');
Route::resource('usuarios','UsuarioController');
Route::resource('roles','RoleController');
//Route::get('/export_pdf/{id}','ProductoController@export_pdf')->name('export_pdf');
Route::get('/export_pdf/{id}','VentaController@export_pdf')->name('factura_pdf');
//Route::get('/export_pdf','ProductoController@export_pdf')->name('export_pdf');

//Route::get('proveedores/{proveedor}','ProveedorController@show')->name('proveedores.show');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('clientes','ClienteController@index')->name('clientes.index')->middleware('permission:clientes.index');

Route::get('/', 'BienvenidoController@index')->name('bienvenido');

Route::get('/prueba/{id}', function (Codedge\Fpdf\Fpdf\Fpdf $fpdf,$id) {
	ob_end_clean();	
    $fpdf->AddPage();
   // $fpdf->SetFont('Courier', 'B', 18);
   // $fpdf->SetFont('Times','',10);
   // $fpdf->Cell(50, 25, 'Hello World!');
    $fpdf->SetXY(10,49);
	$fpdf->SetTextColor(255,0,0);
	$fpdf->SetFont('Times','B',10);
	$fpdf->MultiCell(25,5,$id,0,'L');
    $fpdf->Output();

})->name('prueba');

//informes
Route::get('/informes/productos','InfProductoController@index')->name('inf_productos');

Route::get('/informes/ventas','InfVentaController@index')->name('inf_ventas');
//export pdf
Route::get('/todos_productos','InfProductoController@todos_productos')->name('todos_pdf');



