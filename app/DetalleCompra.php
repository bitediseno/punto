<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    protected $table='detalle_compras';
    protected $primaryKey='cod_detalle_compra';
    public $timestamps=false;
    protected $fillable=['cod_compra','cod_producto','precio_compra','cantidad',
'descuento'];
    protected $guarded=[];
}
