<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
	public $timestamps=false;
    protected $primaryKey = 'cod_producto';
    protected $fillable = [
        'cod_marca','cod_categoria','codigo','nombre_producto','desc_producto','cantidad','precio_compra','precio_venta','estado',
    ];
}
