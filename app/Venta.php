<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
     protected $table='ventas';
    protected $primaryKey='cod_venta';
    public $timestamps=false;
    protected $fillable=['cod_venta','cod_cliente','cod_tipo_venta','numero_factura',
'total_venta','estado'];
    protected $guarded=[];
}
