<?php

namespace App\Http\Controllers;

use App\Marca;
use Illuminate\Http\Request;

use App\Http\Requests\MarcaFormRequest;
use DB;
class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
         if ($request)
        {
        $query=trim($request->get('searchText'));
        $marcas=DB::table('marcas')
        ->where('desc_marca','LIKE','%'.$query.'%')
        ->orderBy('cod_marca', 'desc')
        ->paginate(7);
        return view('marcas.index',["marcas"=>$marcas,"searchText"=>$query]);
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marcas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MarcaFormRequest $request)
    {
        $marca=new Marca;
        $marca->desc_marca=$request->get('desc_marca');
        $marca->estado='Activo';
        $marca->save();
         return redirect('marcas')->with('info','Marca Guardada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return view("marcas.show",["marca"=>Marca::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("marcas.edit",["marca"=>Marca::findOrFail($id)]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function update(MarcaFormRequest $request,  $id)
    {
        $marca=Marca::findOrFail($id);
       $marca->desc_marca=$request->get('desc_marca');
        $marca->estado='Activo';
        $marca->update();
        return redirect('marcas')->with('info','Marca actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $marca=Marca::findOrFail($id);
      $marca->estado='Inactivo';
        $marca->update();
        return redirect('marcas');
    }
}
