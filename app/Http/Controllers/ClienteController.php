<?php

namespace App\Http\Controllers;
use App\Http\Requests\ClienteFormRequest;

use App\Cliente;
use Illuminate\Http\Request;
use DB;
class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('permission:clientes.create')->only(['create','store']);
        $this->middleware('permission:clientes.index')->only(['cliente.index']);
        $this->middleware('permission:clientes.edit')->only(['edit','update']);
        $this->middleware('permission:clientes.show')->only(['show']);
        $this->middleware('permission:clientes.destroy')->only(['destroy']);

    }
    public function index(Request $request)
    {
        if ($request)
        {
       
           
     $query=trim($request->get('searchText'));
        $clientes=DB::table('clientes')
        ->where('cod_tipo_cliente','=','1')
        ->where('nombres','LIKE','%'.$query.'%')
        ->orderBy('cod_cliente', 'desc')
        ->paginate(7);
        return view('clientes.index',["clientes"=>$clientes,"searchText"=>$query]);
    }
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteFormRequest $request)
    {
        $cliente=new Cliente;
        $cliente->nombres=$request->get('razon_social');
        $cliente->ruc=$request->get('ruc');
        $cliente->correo=$request->get('email');
        $cliente->telefono=$request->get('telefono');
        $cliente->cod_tipo_cliente=1;
        $cliente->save();
        /* $proveedor=Proveedor::create($request->all());*/
        return redirect('clientes')->with('info','Cliente Guardado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("clientes.show",["cliente"=>Cliente::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        return view("clientes.edit",["cliente"=>Cliente::findOrFail($id)]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteFormRequest $request,  $id)
    {
        $cliente=Cliente::findOrFail($id);
       $cliente->nombres=$request->get('razon_social');
        $cliente->ruc=$request->get('ruc');
        $cliente->correo=$request->get('email');
        $cliente->telefono=$request->get('telefono');
        $cliente->update();
        return redirect('clientes')->with('info','Cliente actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $cliente=Cliente::findOrFail($id);
        $cliente->delete();
        return redirect('clientes');
    }
}
