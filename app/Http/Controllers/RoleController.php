<?php

namespace App\Http\Controllers;


use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Http\Request;
use DB;
class RoleController extends Controller
{
    
    public function index()
    {
        $roles=Role::paginate();
        return view('roles.index',compact('roles'));
    }

  public function create()
    {
        $permissions=Permission::get();
        return view('roles.create',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role=Role::create($request->all());
        //auth()->user()->assignRoles($request->get('name'));
       /*     auth()->user()->syncRoles($request->get('name'));
        auth()->user()->givePermissionTo('proveedores.update');
        auth()->user()->syncPermissions('proveedores.update');*/
         //role()->syncRoles($request->get('name'));
        $role->permissions()->sync($request->get('permissions'));
        return redirect()->route('roles.edit',$role->id)->with('info','Rol Guardado con exito');
    }

   
    public function show(Role $role)
    {
       // dd($Role->id);
        return view('roles.show',compact('role'));
    }

  
    public function edit(Role $role)
    {
        $permissions=Permission::get();
        $role_permission=DB::table('permission_role as pr')->select('pr.permission_id')->where('pr.role_id', '=',$role->id)->get();
        $role=DB::table('roles as r')
            ->select('r.id as id','r.name','r.slug','r.description','r.special')->where('r.id', '=',$role->id)->first();
        return view('roles.edit',compact('role','permissions','role_permission'));
    }

   
    public function update(Request $request, Role $role)
    {//actualizar roles
       $role->update($request->all());
        //actualizar permisos
        $role->permissions()->sync($request->get('permissions'));
        return redirect()->route('roles.edit',$role->id)->with('info','role actualizado con exito');
    }

   
    public function destroy(Role $role)
    {
        $role->delete();
        return back()->with('info','Rol Eliminado con exito');
    }
}
