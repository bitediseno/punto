<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod_marca'=>'required|max:20',
            'cod_categoria'=>'required|max:20',
            'codigo'=>'required|max:50',
            'nombre_producto'=>'required|max:40',
            'cantidad'=>'required|max:10',
            'precio_compra'=>'required',
            'precio_venta'=>'required',
        ];
    }
}
