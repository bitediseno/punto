<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
	public $timestamps=false;
    protected $primaryKey = 'cod_cliente';
    protected $fillable = [
        'cod_tipo_cliente','nombres','ruc','correo','telefono',
    ];
}
